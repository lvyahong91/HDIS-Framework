package tech.hdis.framework.test;

/**
 * 针对RestfulResponse的JsonPath解析模板的值
 *
 * @author 黄志文
 */
public class ResponseValue {

    public static final String SUCCESS_CODE = "hdis.response.success.success";
    public static final String SUCCESS_VALUE = "成功";
    public static final String BUSINESS_EXCEPTION_CODE = "hdis.response.exception.business";

}
