package tech.hdis.framework.exception.exceptions;


import tech.hdis.framework.exception.properties.ExceptionProperties;

/**
 * 数据验证异常
 *
 * @author 黄志文
 */
public class BusinessException extends AbstractException {
    /**
     * 数据验证异常
     *
     * @param message 异常消息
     */
    public BusinessException(String message) {
        super(ExceptionProperties.BUSINESS_KEY, message);
    }

    /**
     * 数据验证异常
     *
     * @param message   异常消息
     * @param throwable 原本异常
     */
    public BusinessException(String message, Throwable throwable) {
        super(ExceptionProperties.BUSINESS_KEY, message, throwable);
    }
}
