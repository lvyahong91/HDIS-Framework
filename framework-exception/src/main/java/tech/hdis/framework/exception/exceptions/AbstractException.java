package tech.hdis.framework.exception.exceptions;


/**
 * 默认异常，存储相应的错误代码，异常信息，异常栈。
 * 供其他异常继承，不能直接使用。
 *
 * @author 黄志文
 */
public abstract class AbstractException extends RuntimeException {

    /**
     * 错误代码
     */
    protected String errorCode;

    protected AbstractException() {
        super();
    }

    protected AbstractException(String errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    protected AbstractException(String errorCode, String message, Throwable throwable) {
        super(message, throwable);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
