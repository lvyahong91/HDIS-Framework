package tech.hdis.framework.data.jpa.po;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 基础Entity
 *
 * @author 黄志文
 */
@MappedSuperclass
@Getter
@Setter
@ToString
public class JpaDataEntity implements Serializable {

    @PrePersist
    public void onSave() {
        this.updateTime = new Date();
        this.createTime = new Date();
    }

    @PreUpdate
    public void onUpdate() {
        LocalDateTime.now();
    }

    /**
     * 修改时间
     */
    @Id
    @GenericGenerator(name = "idGenerator", strategy = "uuid")
    @GeneratedValue(generator = "idGenerator")
    @Column(name = "uuid", length = 32)
    private String uuid;
    /**
     * 修改时间
     */
    @Column(name = "update_time", columnDefinition = "DATETIME")
    private Date updateTime;
    /**
     * 创建时间
     */
    @Column(name = "create_time", columnDefinition = "DATETIME")
    private Date createTime;
}