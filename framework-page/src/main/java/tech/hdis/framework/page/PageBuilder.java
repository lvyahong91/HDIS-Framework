package tech.hdis.framework.page;

import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

/**
 * 分页工具
 *
 * @author 黄志文
 */
public class PageBuilder {

    /**
     * 获得PageRequest，用于构造分页入参
     *
     * @param pageNum 第几页，从1开始
     * @return spring PageRequest
     */
    public static PageRequest pageRequest(@NonNull Integer pageNum) {
        return new PageRequest(getPageRequestNum(pageNum), PageProperties.PAGE_SIZE_VALUE);
    }

    /**
     * 获得PageRequest，用于构造分页入参
     *
     * @param pageNum    第几页，从1开始
     * @param direction  排序方式
     * @param properties 排序字段
     * @return spring PageRequest
     */
    public static PageRequest pageRequest(@NonNull Integer pageNum, @NonNull Sort.Direction direction, @NonNull String... properties) {
        return new PageRequest(getPageRequestNum(pageNum), PageProperties.PAGE_SIZE_VALUE, direction, properties);
    }

    /**
     * 获得PageRequest，用于构造分页入参
     *
     * @param pageNum  第几页，从1开始
     * @param pageSize 每页几个
     * @return spring PageRequest
     */
    public static PageRequest pageRequest(@NonNull Integer pageNum, @NonNull Integer pageSize) {
        return new PageRequest(getPageRequestNum(pageNum), pageSize);
    }

    /**
     * 获得PageRequest，用于构造分页入参
     *
     * @param pageNum    第几页，从1开始
     * @param pageSize   每页几个
     * @param direction  排序方式
     * @param properties 排序字段
     * @return spring PageRequest
     */
    public static PageRequest pageRequest(@NonNull Integer pageNum, @NonNull Integer pageSize, @NonNull Sort.Direction direction, @NonNull String... properties) {
        return new PageRequest(getPageRequestNum(pageNum), pageSize, direction, properties);
    }

    /**
     * 得到PageInfo，用户返回客户端
     *
     * @param page spring page
     * @return 自定义 PageInfo
     */
    public static PageInfo pageInfo(@NonNull Page page) {
        return new PageInfo(page.getNumber() + 1, page.getSize(), page.getTotalElements(), page.getContent());
    }

    /**
     * 构建PageRequest的pageNum
     *
     * @param pageNum 第几页
     * @return 返回的页数
     */
    private static Integer getPageRequestNum(Integer pageNum) {
        pageNum = pageNum - 1;
        return pageNum < 0 ? 0 : pageNum;
    }
}
