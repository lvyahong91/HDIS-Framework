package tech.hdis.framework.security.manager;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import tech.hdis.framework.security.session.entity.Session;
import tech.hdis.framework.security.session.interfaces.SessionService;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Session管理，用于开发者调用。
 *
 * @author 黄志文
 */
@Slf4j
@Component
public class SessionManager {

    private static SessionService sessionService;

    @Autowired
    public void setSessionService(SessionService sessionService) {
        SessionManager.sessionService = sessionService;
    }

    /**
     * 新增或修改当前线程的session
     *
     * @param userId 用户ID
     * @param role   角色
     */
    public static void saveSession(@NonNull String userId, @NonNull String role) {
        SessionManager.saveSession(userId, new HashSet<>(Arrays.asList(role)), null);
    }

    /**
     * 新增或修改当前线程的session
     *
     * @param userId      用户ID
     * @param roles       角色
     * @param permissions 权限
     */
    public static void saveSession(@NonNull String userId, @NonNull Set<String> roles, Set<String> permissions) {
        Session session = SessionManager.getSession();
        if (session == null) {
            session = new Session();
            session.setToken(UUID.randomUUID().toString().replaceAll("-", ""));
        }
        session.setUserId(userId);
        session.setRoles(roles);
        session.setPermissions(permissions);
        sessionService.saveSession(session);
    }

    /**
     * 得到session
     *
     * @return Session
     */
    public static Session getSession() {
        return sessionService.getSession();
    }

    /**
     * 得到用户ID
     *
     * @return 用户ID
     */
    public static String getUserId() {
        return sessionService.getSession().getUserId();
    }

    /**
     * 登出，删除session
     */
    public static void logout() {
        sessionService.logout();
    }
}
