package tech.hdis.framework.security.session.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.Set;

/**
 * Session
 *
 * @author 黄志文
 */
@Getter
@Setter
@ToString
public class Session {

    /**
     * 会话ID（必填）
     */
    private String token;
    /**
     * 最后访问时间
     */
    private Date lastAccessedTime;
    /**
     * 用户ID
     */
    private String userId;
    /**
     * 角色集
     */
    private Set<String> roles;
    /**
     * 权限集
     */
    private Set<String> permissions;
}
