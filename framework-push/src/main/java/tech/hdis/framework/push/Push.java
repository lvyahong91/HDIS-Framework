package tech.hdis.framework.push;

/**
 * 统一推送接口
 *
 * @author 黄志文
 */
public interface Push {

    /**
     * 消息推送
     *
     * @param content 推送内容
     * @param target  推送目标
     * @return 是否成功
     * @throws Exception 推送异常
     */
    Boolean push(String content, String... target) throws Exception;

    /**
     * 全体消息推送
     *
     * @param content 推送内容
     * @return 是否成功
     * @throws Exception 推送异常
     */
    Boolean pushAll(String content) throws Exception;
}
