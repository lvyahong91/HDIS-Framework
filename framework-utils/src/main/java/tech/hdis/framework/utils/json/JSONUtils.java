package tech.hdis.framework.utils.json;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import lombok.NonNull;

import java.lang.reflect.Type;

/**
 * Json序列化工具
 *
 * @author 黄志文
 */
public class JSONUtils {

    /**
     * 序列化
     *
     * @param object 对象
     * @return 序列化结果
     */
    public static final String toJson(@NonNull Object object) {
        return JSON.toJSONString(object);
    }

    /**
     * @param jsonStr 字符串
     * @param clazz   反序列化Class
     * @param <T>     反序列化泛型
     * @return 反序列化结果
     */
    public static final <T> T fromJson(@NonNull String jsonStr, @NonNull Class<T> clazz) {
        return JSON.parseObject(jsonStr, clazz);
    }

    /**
     * 泛型反序列化<br>
     *
     * @param jsonStr 字符串
     * @param type    TypeReference
     * @return 序列化结果
     */
    public static final Object fromJson(@NonNull String jsonStr, @NonNull Type type) {
        return JSON.parseObject(jsonStr, type);
    }

    /**
     * 判断是否为json字符串
     *
     * @param json 可能的json字符串
     * @return 是否为json
     */
    public static boolean isJsonValid(String json) {
        try {
            JSONObject.parseObject(json);
        } catch (JSONException ex) {
            try {
                JSONObject.parseArray(json);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
}
