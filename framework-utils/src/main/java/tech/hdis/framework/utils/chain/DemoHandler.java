package tech.hdis.framework.utils.chain;


/**
 * 抽象责任链模式-Demo
 *
 * @author 黄志文
 */
public class DemoHandler extends AbstractHandlerChain<Object, Object> {

    private static DemoHandler instance = new DemoHandler();

    private DemoHandler() {
    }

    public static DemoHandler getInstance() {
        return instance;
    }

    @Override
    public Object doHandler(Object params) {
        return null;
    }
}