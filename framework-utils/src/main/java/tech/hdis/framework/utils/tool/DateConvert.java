package tech.hdis.framework.utils.tool;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间类型转化器
 *
 * @author 黄志文
 */
public class DateConvert {
    /**
     * Date转化为LocalDateTime
     *
     * @param date Date
     * @return LocalDateTime
     */
    public static LocalDateTime Date2LocalDateTime(Date date) {
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        return LocalDateTime.ofInstant(instant, zone);
    }

    /**
     * UTC时间转北京时间
     *
     * @param utc    utc时间
     * @param format 格式化
     * @return 北京时间
     * @throws ParseException
     */
    public static Date UTC2CST(String utc, String format) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Date date = simpleDateFormat.parse(utc);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR) + 8);
        return calendar.getTime();
    }

    /**
     * 普通时间转化
     *
     * @param dateStr 时间字符串
     * @param format  格式化
     * @return 北京时间
     * @throws ParseException
     */
    public static Date parse(String dateStr, String format) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        Date date = simpleDateFormat.parse(dateStr);
        return date;
    }
}
