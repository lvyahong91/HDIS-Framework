package tech.hdis.framework.utils.tool;

/**
 * 性能测试时间记录工具
 *
 * @author 黄志文
 */
public class Timer {

    private Long startTime = 0L;

    public Timer getTimer() {
        return new Timer();
    }

    public Timer start() {
        this.startTime = System.currentTimeMillis();
        return this;
    }

    public Long costTimeMillis() {
        return startTime - System.currentTimeMillis();
    }

    public Long costTimeSeconds() {
        return (startTime - System.currentTimeMillis()) / 1000;
    }
}
