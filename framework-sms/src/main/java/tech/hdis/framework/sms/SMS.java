package tech.hdis.framework.sms;

import java.util.Map;

/**
 * 短信
 *
 * @author 黄志文
 */
public interface SMS {
    /**
     * 发送短信
     *
     * @param number       电话号码
     * @param templateCode 短信模板号
     * @param params       发送参数
     * @return 是否发送成功
     * @throws Exception 验证码异常
     */
    boolean sendSMS(String number, String templateCode, Map<String, String> params) throws Exception;
}
