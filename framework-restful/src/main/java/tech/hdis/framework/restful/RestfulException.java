package tech.hdis.framework.restful;

/**
 * 服务之间调用异常
 *
 * @author 黄志文
 */
public class RestfulException extends RuntimeException {

    public RestfulException(String url) {
        super("Restful request fail!.The url is : " + url);
    }

    public RestfulException(String url, Throwable cause) {
        super("Restful request fail!.The url is : " + url, cause);
    }
}
