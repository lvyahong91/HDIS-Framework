package tech.hdis.framework.data.mongo.po;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.Date;

/**
 * MongoDB基础Entity
 *
 * @author 黄志文
 */
@Getter
@Setter
@ToString
public class MongoDataEntity implements Serializable {

    public void onSave() {
        this.updateTime = new Date();
        this.createTime = new Date();
    }

    public void onUpdate() {
        this.updateTime = new Date();
    }

    /**
     * 修改时间
     */
    @Id
    private String uuid;
    /**
     * 修改时间
     */
    @Field("update_time")
    private Date updateTime;
    /**
     * 创建时间
     */
    @Field("create_time")
    private Date createTime;
}