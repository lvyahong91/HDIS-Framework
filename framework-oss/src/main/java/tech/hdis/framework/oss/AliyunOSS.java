package tech.hdis.framework.oss;

/**
 * 统一对象存储服务接口
 *
 * @author 黄志文
 */
public interface AliyunOSS extends OSS {
    /**
     * 获取可用的合法上传URL
     *
     * @param fileName 文件名称
     * @return URL字符串
     */
    String getUploadUrl(String directory, String fileName);

    /**
     * 获取可用的合法下载URL
     *
     * @param fileName 文件名称
     * @return URL字符串
     */
    String getDownloadUrl(String directory, String fileName);
}
